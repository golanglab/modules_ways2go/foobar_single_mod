package fbsextwo

import "testing"

func TestFoobarSingleAbout(t *testing.T) {
	expected := "I am from foobar single module repo example 2."
	actual := About()
	if actual != expected {
		t.Errorf("Expected: %s Actual: %s", expected, actual)
	}
}
