package main

import (
	"fmt"
	"gitlab.com/golanglab/modules_ways2go/foobar_single_mod/pkg/fbsexone"
	"gitlab.com/golanglab/modules_ways2go/foobar_single_mod/pkg/fbsextwo"
)

func main() {
	fmt.Println(fbsexone.About())
	fmt.Println(fbsextwo.About())
}
